import { PROJECT_REDUCER_LOAD_DATA, PROJECT_REDUCER_UPDATE_SELECTED_FORM, PROJECT_REDUCER_UPDATE_PREVIEW } from "./actionTypes";

export function projectLoadData (forms) {
  return {
    type: PROJECT_REDUCER_LOAD_DATA,
    payload: {
      forms
    }
  };
}

export function projectUpdateSelectedForm (form) {
  return {
    type: PROJECT_REDUCER_UPDATE_SELECTED_FORM,
    payload: {
      form
    }
  };
}

export function projectUpdatePreview (preview) {
  return {
    type: PROJECT_REDUCER_UPDATE_PREVIEW,
    payload: {
      preview
    }
  };
}
