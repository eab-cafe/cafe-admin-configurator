import { PREVIEW_REDUCER_SET_FOCUS_QUESTION, PREVIEW_REDUCER_REMOVE_FOCUS_QUESTION } from "./actionTypes";

export function previewSetFocusQuestion(question) {
  return {
    type: PREVIEW_REDUCER_SET_FOCUS_QUESTION,
    payload: {
      question
    }
  }
}

export function previewRemoveFocusQuestion() {
  return {
    type: PREVIEW_REDUCER_REMOVE_FOCUS_QUESTION
  }
}
