import { PREVIEW_REDUCER_SET_FOCUS_QUESTION, PREVIEW_REDUCER_REMOVE_FOCUS_QUESTION } from "../actions/actionTypes";

const initialState = {
  focusQuestion: null,
}

const previewReducer = ( state = initialState, { type, payload } ) => {
  switch (type) {
    case PREVIEW_REDUCER_SET_FOCUS_QUESTION:
      return {
        ...state,
        focusQuestion: payload.question
      };
    case PREVIEW_REDUCER_REMOVE_FOCUS_QUESTION:
      return {
        ...state,
        focusQuestion: null
      };
    default:
      return state;
  }
}

export default previewReducer;
