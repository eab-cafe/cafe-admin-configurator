import { combineReducers } from 'redux';
import projectReducer from './projectReducer';
import previewReducer from './previewReducer';

const rootReducer = combineReducers({
  projectReducer, previewReducer
})

export default rootReducer;
