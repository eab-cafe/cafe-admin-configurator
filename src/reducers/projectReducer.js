import { PROJECT_REDUCER_LOAD_DATA, PROJECT_REDUCER_UPDATE_SELECTED_FORM, PROJECT_REDUCER_UPDATE_PREVIEW } from "../actions/actionTypes";

const initialState = {
  forms: [],
  selectedForm: null,
  preview: null
}

const projectReducer = (state = initialState, {type, payload}) => {
  switch (type) {
    case PROJECT_REDUCER_UPDATE_PREVIEW:
      return {
        ...state,
        preview: payload.preview
      };
    case PROJECT_REDUCER_UPDATE_SELECTED_FORM:
    // Refresh the preview and selected form
      return {
        ...state,
        selectedForm: payload.form,
        preview: payload.form
      };
    case PROJECT_REDUCER_LOAD_DATA:
      return {
        ...state,
        forms: payload.forms,
        selectedForm: payload.forms[0],
        preview: payload.forms[0]
      };
    default:
      return state;
  }
}

export default projectReducer;
