export const SECTION = "SECTION";
export const TITLE = "TITLE";
export const TEXT_INPUT = "TEXT_INPUT";
export const SELECTOR = "SELECTOR";
export const CHECK_BOX = "CHECK_BOX";
export const DROP_DOWN = "DROP_DOWN";
export const DATE = "DATE";
export const SCALE = "SCALE";
export const SLIDER = "SLIDER";
export const FILE_UPLOAD = "FILE_UPLOAD";
export const NONE = "NONE";

export const singlechoice = "SELECTOR"
