import React, { cloneElement } from 'react';
import styles from './index.module.css';

import Header from '../../components/Header';

export default function Main(props) {
  const { children } = props;
  return (
    <div className={styles.container}>
      {
        [
          <Header className={styles.header} key="header"/>,
          cloneElement(children, { className: styles.main, key: 'main' })
        ]
      }
    </div>
  )
}
