import React, {Component} from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';
import classnames from 'classnames';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import TextField from '@material-ui/core/TextField';
import AddIcon from '@material-ui/icons/Add';
// import IconButton from '@material-ui/core/IconButton';
// import { DragDropContext, Droppable } from 'react-beautiful-dnd';
import { Element } from 'react-scroll';

import styles from './QuestionForm.module.css';
import i18n from '../../../i18n';
import QuestionPage from './QuestionPage';

export default class QuestionForm extends Component{
  constructor(props) {
    super(props);
    this.state = {
      tabIndex: 0
    }
    this.tabChange = this.tabChange.bind(this);
    this.onDragEnd = this.onDragEnd.bind(this);
    this.updateForm = this.updateForm.bind(this);
  }

  onDragEnd(result) {
    // const { selectedForm, selectedFormEdit } = this.props;
    // const { source, destination } = result;
    // dropped outside the list
    // if (!destination) {
    //   return;
    // }

    // This function make changes in the selectedForm in state. Only changing selectedForm's inside value.
    // Didn't change selectedForm's address so it will works.
    // const selectedFormReorder = () => {
    //   const destinationPage = selectedForm.pages.find((page) => page.label === destination.droppableId);
    //   const sourcePage = selectedForm.pages.find((page) => page.label === destination.droppableId);
    //
    //   const destinationQuestionList = destinationPage.questions;
    //   const sourceQuestionList = sourcePage.questions;
    //
    //   const [temp] = sourceQuestionList.splice(source.index, 1);
    //   destinationQuestionList.splice(destination.index, 0, temp);
    // }
    // selectedFormReorder();
    //
    // selectedFormEdit(selectedForm);
  }

  updateForm(data) {
    const {tabIndex} = this.state;
    const {dataUpdate} = this.props;
    dataUpdate({...data, tabIndex});
  }

  loadingScreen() {
    return (
      <CircularProgress className={styles.progress} />
    );
  }

  tabChange(event, value) {
    if (value === "Add") {
      // TODO: Add page
      console.log(value);
    } else {
      this.setState({tabIndex: value});
    }
  }

  formGenerate() {
    const { selectedForm } = this.props;
    const { tabIndex } = this.state;
    const titleTextField = (
      <div className={styles.title}>
        {selectedForm.title}
      </div>
    )

    // <TextField
    //   className={styles.title}
    //   label={i18n({path:"questionForm.title.label"})}
    //   InputProps={{
    //     classes: {
    //       input: styles.textInput,
    //     },
    //   }}
    //   defaultValue={selectedForm.title}
    // />

    const descriptionTextField = (
      <TextField
        className={styles.description}
        label={i18n({path:"questionForm.description.label"})}
        InputProps={{
          classes: {
            input: styles.textInput,
          },
        }}
        defaultValue={selectedForm.description}
        fullWidth
      />
    )

    const tabListJSX = (<Tabs value={tabIndex} onChange={this.tabChange}>
      {
        selectedForm.pages.map(page => {
          return (
            <Tab label={page.title} key={page.title} />
          )
        })
      }
      <Tab icon={<AddIcon />} key="Add" value="Add"/>
    </Tabs>)

    return (<div className={styles.formContainer}>
      <div className={styles.questionForm}>
        <div className={styles.topBar}>
          {titleTextField}
          {descriptionTextField}
        </div>
        {tabListJSX}
        <div className={styles.tabContainerWrapper}>
          <QuestionPage
            page={selectedForm.pages[tabIndex]}
            updateForm={this.updateForm}/>
        </div>
      </div>
    </div>);
  }

  render() {
    const { className, selectedForm } = this.props;
    return (
      <Element id="QuestionFormSection" className={classnames(className, styles.container)}>
        {
          selectedForm ? this.formGenerate() : this.loadingScreen()
        }
      </Element>
    )
  }
}

// <div className={styles.questionAddButton}>
//   <IconButton color="primary" className={styles.button} component="span">
//     <AddIcon />
//   </IconButton>
// </div>
