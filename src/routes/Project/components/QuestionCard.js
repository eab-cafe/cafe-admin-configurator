import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import Paper from '@material-ui/core/Paper';
import TextField from '@material-ui/core/TextField';
import DragIcon from '@material-ui/icons/DragIndicator';
import DeleteIcon from '@material-ui/icons/Delete';
import FileCopyIcon from '@material-ui/icons/FileCopy';
import VisibilityIcon from '@material-ui/icons/Visibility';
import VisibilityOffIcon from '@material-ui/icons/VisibilityOff';
import BallotIcon from '@material-ui/icons/Ballot';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Popover from '@material-ui/core/Popover';
import { Element, scroller } from 'react-scroll';
import Checkbox from '@material-ui/core/Checkbox';

import { SECTION, TITLE, TEXT_INPUT, SELECTOR, CHECK_BOX, DROP_DOWN, DATE, SCALE, SLIDER, FILE_UPLOAD, NONE} from "../../../constants/questionType";
import styles from './QuestionCard.module.css';
import QuestionTextInput from './questionType/QuestionTextInput';
import QuestionSelector from './questionType/QuestionSelector';
import QuestionNone from './questionType/QuestionNone';
import i18n from '../../../i18n';
import { previewSetFocusQuestion, previewRemoveFocusQuestion } from '../../../actions/previewAction';

const dropDownQuestionType = [
  {
    label: "SECTION",
    icon: null,
  },
  {
    label: "TITLE",
    icon: null,
  },
  {
    label: "TEXT_INPUT",
    icon: null,
  },
  {
    label: "SELECTOR",
    icon: null,
  },
  {
    label: "CHECK_BOX",
    icon: null,
  },
  {
    label: "DROP_DOWN",
    icon: null,
  },
  {
    label: "DATE",
    icon: null,
  },
  {
    label: "SCALE",
    icon: null,
  },
  {
    label: "SLIDER",
    icon: null,
  },
  {
    label: "FILE_UPLOAD",
    icon: null,
  },
];

class QuestionCard extends Component {
  constructor(props) {
    super(props);

    this.state = {
      anchorE: null,
      dataValidatePopover: false,
      isDeleteDialogOpen: false
    };
    this.dropDownClick = this.dropDownClick.bind(this);
    this.dropDownClose = this.dropDownClose.bind(this);
    this.mandatoryBtnClick = this.mandatoryBtnClick.bind(this);
    this.menuItemClick = this.menuItemClick.bind(this);
    this.updateQuestion = this.updateQuestion.bind(this);
    this.copyQuestion = this.copyQuestion.bind(this);
    this.removeQuestion = this.removeQuestion.bind(this);
    this.handleTitleBlur = this.handleTitleBlur.bind(this);
    this.handleTitleChange = this.handleTitleChange.bind(this);
    this.disableClick = this.disableClick.bind(this);
    this.deleteDialogOpen = this.deleteDialogOpen.bind(this);
    this.deleteDialogClose = this.deleteDialogClose.bind(this);
    this.mouseEnterQuestion = this.mouseEnterQuestion.bind(this);
    this.mouseLeaveQuestion = this.mouseLeaveQuestion.bind(this);
    this.setDataValidatePopover = this.setDataValidatePopover.bind(this);
    this.setDataValidateMinLength = this.setDataValidateMinLength.bind(this);
    this.setDataValidateCheckbox = this.setDataValidateCheckbox.bind(this);
  }

  dropDownClick(event) {
    this.setState({ anchorEl: event.currentTarget });
  }

  dropDownClose(event) {
    this.setState({ anchorEl: null });
  }

  mandatoryBtnClick() {
    const { question } = this.props;
    if (question.isMandatory === undefined) {
      question.isMandatory = false;
    }
    question.isMandatory = !question.isMandatory;
    this.updateQuestion({ updateData: question });
  }

  menuItemClick(questionType) {
    // Will add more later
    const { question } = this.props;
    question.questionType = questionType.label;
    this.setState({ anchorEl: null });
    this.updateQuestion({ updateData: question });
  }

  handleTitleBlur(event, question) {
    question.title = event.target.value;
    this.updateQuestion({ updateData: question });
  }

  handleTitleChange(event, question) {
    question.title = event.target.value;
    this.updateQuestion({ updateData: question,isPreview: true});
  }

  disableClick() {
    const { question, removeFocusQuestion } = this.props;
    question.isDisable = !question.isDisable;
    removeFocusQuestion();
    this.updateQuestion({ updateData: question });
  }

  copyQuestion() {
    const { functionPackage, question, questionIndex } = this.props;
    functionPackage.add({question, questionIndex });
  }

  removeQuestion() {
    // Remove will need to call Section to remove the question.
    const { functionPackage, question } = this.props;
    functionPackage.remove({id: question._id})
  }

  updateQuestion(data) {
    const { functionPackage, question } = this.props;
    functionPackage.update({...data, questionId: question._id});
  }

  deleteDialogOpen() {
    this.setState({isDeleteDialogOpen: true});
  }

  deleteDialogClose() {
    this.setState({isDeleteDialogOpen: false});
  }

  mouseEnterQuestion() {
    const { question, focusQuestion } = this.props;
    if (!question.isDisable) {
      scroller.scrollTo(question._id, {
        duration: 400,
        smooth: true,
        offset: -(window.innerHeight / 2),
        containerId: 'PreviewTemplate'
      });
      focusQuestion(question);
    }
  }

  mouseLeaveQuestion() {
    const { removeFocusQuestion } = this.props;
    removeFocusQuestion();
  }

  mouseClickQuestion(question) {
    scroller.scrollTo(`${question._id} card`, {
      duration: 400,
      smooth: true,
      offset: -(window.innerHeight / 2),
      containerId: 'QuestionFormSection'
    });
  }

  setDataValidatePopover(flag) {
    this.setState({dataValidatePopover: flag})
  }

  setDataValidateCheckbox (e) {
    const {question} = this.props;
    question.isNum = e.target.checked;
    this.updateQuestion({ updateData: question });
  }

  setDataValidateMinLength (e) {
    const {question} = this.props;
    question.minLength = parseInt(e.target.value)
    console.log(question);
    this.updateQuestion({ updateData: question });
  }

  deleteDialog () {
    const { isDeleteDialogOpen } = this.state;
    return (<Dialog
      open={isDeleteDialogOpen}
      onClose={this.deleteDialogClose}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
    >
      <DialogTitle id="alert-dialog-title">{"Delete Dialog"}</DialogTitle>
      <DialogContent>
        <DialogContentText id="alert-dialog-description">
          Are you Sure you want to delete?
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button onClick={this.removeQuestion} color="primary">
          Delete
        </Button>
        <Button onClick={this.deleteDialogClose} color="primary" autoFocus>
          Cancel
        </Button>
      </DialogActions>
    </Dialog>)
  }

  cardHeader() {
    const { question, questionIndex } = this.props;
    const { anchorEl } = this.state;

    const menuItemJSX = dropDownQuestionType.map((questionType) => (
      <MenuItem
        onClick={() => this.menuItemClick(questionType)}
        selected={question.questionType === questionType.label}
        key={questionType.label}>
          {i18n({path:`questionType.${questionType.label}`})}
      </MenuItem>
    ));

    return (
      <Element
       className={styles.headerBar} key="header" >
        <TextField
          className={styles.questionTitle}
          label={`${i18n({path:"questionForm.questionTitle.label"})} ${questionIndex + 1}`}
          defaultValue={question.title}
          onChange={(e) =>this.handleTitleChange(e, question)}
          onBlur={(e) =>this.handleTitleBlur(e, question)}
        />
        <Button
          className={styles.dropDownBtn}
          aria-owns={anchorEl ? 'simple-menu' : undefined}
          aria-haspopup="true"
          onClick={this.dropDownClick}
        >
          {i18n({path:`questionType.${question.questionType}`})}
        </Button>
        <Menu
          id="simple-menu"
          anchorEl={anchorEl}
          open={Boolean(anchorEl)}
          onClose={this.dropDownClose}
        >
          {menuItemJSX}
        </Menu>
      </Element>
    )
  }

  cardContent() {
    const { question, functionPackage } = this.props;
    switch (question.questionType) {
      case SECTION:
        break;
      case TITLE:
        break;
      case TEXT_INPUT:
        return (<QuestionTextInput
          question={question}
          functionPackage={functionPackage}
          key="content"/>)
      case SELECTOR:
        return (<QuestionSelector
          question={question}
          functionPackage={functionPackage}
          key="content"/>);
      case CHECK_BOX:
        break;
      case DROP_DOWN:
        break;
      case DATE:
        break;
      case SCALE:
        break;
      case SLIDER:
        break;
      case FILE_UPLOAD:
        break;
      case NONE:
        return <QuestionNone key="content"/>
      default:
    }
    return null;
  }

  cardFooter() {
    const { question } = this.props;
    const { dataValidatePopover } = this.state;
    const asteriskBtnColor = question.isMandatory ? 'secondary' : 'default';
    const disableIconColor = question.isDisable ? 'secondary' : 'default';
    const disableIcon = question.isDisable ? (<VisibilityOffIcon />) : <VisibilityIcon />;
    const minLength = question.minLength || 0;

    return (
      <div className={styles.footerBar} key="footer">
        <IconButton
          color={disableIconColor}
          className={styles.button}
          onClick={this.disableClick}>
          {disableIcon}
        </IconButton>
        <IconButton
          buttonRef={node => {this.anchorEl = node}}
          className={styles.button}
          onClick={()=>this.setDataValidatePopover(true)}>
          <BallotIcon />
        </IconButton>
        <Popover
          open={dataValidatePopover}
          anchorEl={this.anchorEl}
          onClose={()=>this.setDataValidatePopover(false)}
          anchorOrigin={{
            vertical: "top",
            horizontal: "center",
          }}
          transformOrigin={{
            vertical: "top",
            horizontal: "center",
          }}>
          <div className={styles.dataValidateContainer}>
            <div>
              <Checkbox checked={question.isNum} onChange={this.setDataValidateCheckbox}/>
              Is numeric?
            </div>
            <TextField
              type="number"
              label="Length of Text"
              defaultValue={minLength}
              onBlur={(e)=>this.setDataValidateMinLength(e)}
            />
          </div>
        </Popover>
        <IconButton
          className={styles.button}
          onClick={this.copyQuestion}>
          <FileCopyIcon />
        </IconButton>
        <IconButton
          color={asteriskBtnColor}
          onClick={this.mandatoryBtnClick}>
          <span className={classnames(styles.asteriskBtn, "icon-asterisk-black-star-shape")}/>
        </IconButton>
        <IconButton
          className={styles.button}
          onClick={this.deleteDialogOpen}>
          <DeleteIcon />
        </IconButton>
      </div>
    )
  }

  render () {
    const { className, provided, question } = this.props;
    return (
      <Element
      name={`${question._id} card`}
      >
      <div
        className={classnames(className, styles.cardWrapper)}
        ref={provided.innerRef}
        {...provided.draggableProps}
        {...provided.dragHandleProps}>
        {this.deleteDialog()}
        <div className={styles.dragBox} key="dragBox">
          <DragIcon />
        </div>
        <Paper
          className={styles.cardContainer}
          key="questionContent"
          onMouseEnter={this.mouseEnterQuestion}
          onMouseLeave={this.mouseLeaveQuestion}
          onClick={() => this.mouseClickQuestion(question)}>
          {
            [
              this.cardHeader(),
              this.cardContent(),
              <Divider key="Divider"/>,
              this.cardFooter(),
            ]
          }
        </Paper>
      </div>
      </Element>
    )
  }
}

// const mapStateToProps = state => ({
//   preview: state.projectReducer.preview
// });

const mapDispatchToProps = dispatch => {
  return {
    focusQuestion: (data) => dispatch(previewSetFocusQuestion(data)),
    removeFocusQuestion: () => dispatch(previewRemoveFocusQuestion())
  }
}

export default connect(null, mapDispatchToProps)(QuestionCard);

QuestionCard.propTypes = {
  question: PropTypes.object,
  questionIndex: PropTypes.number,
  functionPackage: PropTypes.object
}
