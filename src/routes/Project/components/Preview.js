import React, { Component } from 'react';
import { connect } from 'react-redux';
import classnames from 'classnames';
import Paper from '@material-ui/core/Paper';
import IconButton from '@material-ui/core/IconButton';
import OpenInNewButton from '@material-ui/icons/OpenInNew';
import PreviewTemplate from '../../../components/PreviewTemplate';
import CircularProgress from '@material-ui/core/CircularProgress';

import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';

import styles from './Preview.module.css';

class Preview extends Component {
  constructor(props) {
    super(props);
    this.state = {
      openDialog: false
    }
    this.setOpenDialog = this.setOpenDialog.bind(this);
  }

  setOpenDialog(flag) {
    this.setState({openDialog: flag});
  }

  render() {
    const { preview, className } = this.props;
    const { openDialog } = this.state;
    // const [openDialog, setOpenDialog] = React.useState(false);

    const previewPopover = (
      <Dialog
          open={openDialog}
          onClose={()=> this.setOpenDialog(false)}
          scroll="paper"
          maxWidth="lg"
          aria-labelledby="scroll-dialog-title"
        >
          <DialogContent>
            <PreviewTemplate preview={preview} isFullscreen/>
          </DialogContent>
        </Dialog>
    )
    return (
      <div className={classnames(className, styles.previewContainer)}>
        {previewPopover}
        <div className={styles.previewTitle}>
          Live Preview
        </div>
        <div className={styles.topBar}>
          <IconButton className={styles.expandBtn} onClick={()=>this.setOpenDialog(true)}>
            <OpenInNewButton/>
          </IconButton>
        </div>
        <Paper id="PreviewTemplate" className={styles.previewSection}>
          {preview ?
            <PreviewTemplate preview={preview}/> :
            <div className={styles.progressWrapper}>
              <CircularProgress className={styles.progress} />
            </div>
          }
        </Paper>
        <div className={styles.bottomBar}>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  preview: state.projectReducer.preview
});

// const mapDispatchToProps = dispatch => {
//   return {
//     loadAPIData: (data) => dispatch(projectLoadData(data)),
//     updateSelectedForm: (data) => dispatch(projectUpdateSelectedForm(data)),
//     updatePreview: (data) => dispatch(projectUpdatePreview(data)),
//   }
// }

export default connect(mapStateToProps, null)(Preview);
