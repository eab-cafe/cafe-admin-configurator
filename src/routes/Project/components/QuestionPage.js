import React, { Component } from 'react';
import classnames from 'classnames';
import { DragDropContext } from 'react-beautiful-dnd';
import _ from 'lodash';
import AddIcon from '@material-ui/icons/Add';
import IconButton from '@material-ui/core/IconButton';
import { Element } from 'react-scroll';

import QuestionSection from './QuestionSection';
import styles from './QuestionPage.module.css';

export default class QuestionPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      page: props.page
    };
    this.updatePage = this.updatePage.bind(this);
    this.addSection = this.addSection.bind(this);
    this.questionOnDragEnd = this.questionOnDragEnd.bind(this);
  }

  questionOnDragEnd(result) {
    const { page } = this.state;
    const { source, destination } = result;
    // const { questions } = section;
    if (!destination) {
      return;
    };
    const { sections } = page;
    const sourceSection = _.find(sections, {_id: source.droppableId});
    const destinationSection = _.find(sections, {_id: destination.droppableId});

    const [sourceQuestion] = sourceSection.questions.splice(source.index, 1);
    destinationSection.questions.splice(destination.index, 0, sourceQuestion);
    this.updatePage({updateData: page});
  }

  addSection() {
    const { page } = this.state;

    const tempSection = {
        title: `New Section ${page.sections.length + 1}`,
        questions: [],
        _id: `New Section ${page.sections.length + 1}`
      };
    page.sections.push(tempSection);

    this.updatePage({
      updateData: page
    })
  }

  updatePage(data) {
    const { updateForm } = this.props;
    const { page } = this.state;
    updateForm({...data, pageId: page._id});
  }

  render() {
    const { sections } = this.state.page;
    const { className } = this.props;
    const sectionsJSX = sections.map((section, index) => {
      return (
        <QuestionSection
          className={styles.section}
          section={section}
          updatePage={this.updatePage}
          key={section._id}/>
      )
    });

    return (<DragDropContext onDragEnd={this.questionOnDragEnd}>
      <Element className={classnames(className, styles.sectionContainer)}>
        {sectionsJSX}
      </Element>
      <div className={styles.addSectionBar}>
        <IconButton color="primary" className={styles.button} component="span" onClick={this.addSection}>
          <AddIcon />
        </IconButton>
        <div className={styles.addSection}>
          Add Section
        </div>
      </div>
    </DragDropContext>);

    // Dragable
    // return (
    //   <DragDropContext onDragEnd={this.onDragEnd}>
    //     <Droppable droppableId={page._id}>
    //       {(provided, snapshot) => (
    //         <div ref={provided.innerRef}>
    //           {sectionsJSX}
    //         </div>
    //       )}
    //     </Droppable>
    //   </DragDropContext>
    // )
  }
}
