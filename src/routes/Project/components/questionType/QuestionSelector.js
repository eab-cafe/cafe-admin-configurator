import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import TextField from '@material-ui/core/TextField';
import ClearIcon from '@material-ui/icons/Clear';
import IconButton from '@material-ui/core/IconButton';

import baseStyles from './BaseQuestion.module.css';
import styles from './QuestionSelector.module.css';


export default class QuestionSelector extends Component {
  constructor(props) {
    super(props);

    const { answers = {} } = props.question;

    this.state = {
      answers,
    }
    this.handleOptionTextChange = this.handleOptionTextChange.bind(this);
    this.handleOptionTextBlur = this.handleOptionTextBlur.bind(this);
    this.addOptions = this.addOptions.bind(this);
    this.removeOptions = this.removeOptions.bind(this);
  }

  handleOptionTextChange(event, index) {
    const { functionPackage, question } = this.props;
    const { choices } = this.state.answers;
    choices[index].text = event.target.value;
    functionPackage.update({updateData: question, questionId: question._id, isPreview: true});
  }

  handleOptionTextBlur(event, index) {
    const { functionPackage, question } = this.props;
    const { choices } = this.state.answers;
    choices[index].text = event.target.value;
    functionPackage.update({updateData: question, questionId: question._id});
  }

  addOptions(event) {
    const { functionPackage, question } = this.props;
    const { answers } = this.state;
    if (event.target.value === "ADD_OPTION"){
      if (answers.choices === undefined) {
        // Init the choices if answer choices is empty
        answers.choices = [];
      }
      answers.choices.push({
        text:`Option ${answers.choices.length + 1}`
      });
      functionPackage.update({updateData: question, questionId: question._id});
    }
  }

  removeOptions(index) {
    // const { radioGroup } = this.state;
    // radioGroup.splice(index, 1);
    // // this.setState({radioGroup});
    const { answers } = this.state;
    answers.choices = answers.choices.splice(index, 1);
  }

  render() {
    const { className } = this.props;
    const { answers } = this.state;

    const { choices = [] } = answers;

    const radioLabel = (choice, index) => (
      <div className={styles.radioLabelRow}>
        <TextField
          defaultValue={choice.text}
          onChange={(e) => this.handleOptionTextChange(e, index)}
          onBlur={(e) => this.handleOptionTextBlur(e, index)}
          fullWidth
        />
        <IconButton
          color="primary"
          className={styles.button}
          component="span"
          onClick={() => this.removeOptions(index)}>
          <ClearIcon />
        </IconButton>
      </div>
    );

    const formControlJSX = choices.map((choice, index)=>
      (<FormControlLabel
        className={classnames(baseStyles.questionRow, styles.questionRow)}
        control={<Radio />}
        label={radioLabel(choice, index)}
        key={choice.text}/>)
    );

    return (
    <RadioGroup
      className={classnames(baseStyles.questionContainer, className)}
      onChange={this.addOptions}
      value="">

        {formControlJSX}

        <FormControlLabel
          className={classnames(baseStyles.questionRow, styles.questionRow)}
          control={<Radio />}
          label="Add Option"
          value="ADD_OPTION"
          key="Add Option"/>
    </RadioGroup>)
  }
}

QuestionSelector.propTypes = {
  question: PropTypes.object,
  functionPackage: PropTypes.object
}
