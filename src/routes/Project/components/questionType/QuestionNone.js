import React from 'react';
import classnames from 'classnames';

import i18n from '../../../../i18n';
import styles from './QuestionNone.module.css';
import baseStyles from './BaseQuestion.module.css';

export default function QuestionNone(props) {
  return (<div className={classnames(baseStyles.questionContainer, styles.noneContent)}>
    {i18n({path: 'questionTypeContent.none'})}
  </div>)
}
