import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';

import baseStyles from './BaseQuestion.module.css';
import styles from './QuestionTextInput.module.css';


export default class QuestionTextInput extends Component {
  constructor(props) {
    super(props);

    this.radioGroup = [{
      label: "Single Input",
      value: "TEXT_INPUT"
    }, {
      label: "Text Field",
      value: "TEXT_FIELD"
    }];

    // Binding
    this.radioOnChange = this.radioOnChange.bind(this);
  }

  radioOnChange(event) {
    const { question, functionPackage } = this.props;
    question.answers.type = event.target.value;
    functionPackage.update({updateData: question, questionId: question._id});
  }

  render() {
    const { className, question } = this.props;
    const { type: answersType } = question.answers;

    const formControlJSX = this.radioGroup.map((radioRow)=>
      (<FormControlLabel
        className={classnames(baseStyles.questionRow, styles.questionRow)}
        value={radioRow.value}
        control={<Radio />}
        label={radioRow.label}
        key={radioRow.label}/>)
    );

    return (<RadioGroup
      className={classnames(baseStyles.questionContainer, className)}
      value={answersType}
      onChange={this.radioOnChange}>
      {formControlJSX}
    </RadioGroup>)
  }
}

QuestionTextInput.propTypes = {
  question: PropTypes.object,
  functionPackage: PropTypes.object
}
