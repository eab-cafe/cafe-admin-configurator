import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { Droppable, Draggable } from 'react-beautiful-dnd';
import AddIcon from '@material-ui/icons/Add';
import IconButton from '@material-ui/core/IconButton';
import TextField from '@material-ui/core/TextField';
import { Element } from 'react-scroll';

import QuestionCard from './QuestionCard';
import styles from './QuestionSection.module.css';

export default class QuestionSection extends Component {
  constructor(props) {
    super(props);
    this.state = {
      section: props.section
    }
    this.questionOnDragEnd = this.questionOnDragEnd.bind(this);
    this.addQuestion = this.addQuestion.bind(this);
    this.updateSection = this.updateSection.bind(this);
    this.removeQuestion = this.removeQuestion.bind(this);
    this.handleSectionTitleBlur = this.handleSectionTitleBlur.bind(this);
    this.handleSectionTitleChange = this.handleSectionTitleChange.bind(this);
  }

  static getDerivedStateFromProps(props, state) {
    if (props.section !== state.section) {
      this.setState({section: props.section});
    }
    return null;
  }

  questionOnDragEnd(result) {
    // const { selectedForm, selectedFormEdit } = this.props;
    const { section } = this.state;
    const { source, destination } = result;
    const { questions } = section;
    if (!destination) {
      return;
    };

    // const destinationPage = questions;
    const [temp] = questions.splice(source.index, 1);
    questions.splice(destination.index, 0, temp);
    this.updateSection({updateData: section});
  }

  addQuestion({question = null, questionIndex}) {
    const { section } = this.state;
    if (question) {
      section.questions.splice(questionIndex + 1, 0, {...question, _id:(question._id + "1")});
    } else {
      const tempQuestion = {
        _id: `newID ${section._id + section.questions.length + 1}`,
        title: `Text Box Question ${section.questions.length + 1}`,
        questionType: "SELECTOR",
        answers: {},
        logicRules: [],
        isDisable: false,
        createdAt: 43242342342,
        updatedAt: 2343243242
      };
      section.questions.push(tempQuestion);
    }
    this.updateSection({
      updateData: section
    })
  }

  handleSectionTitleBlur(event, section) {
    section.title = event.target.value;
    this.updateSection({ updateData: section });
  }

  handleSectionTitleChange(event, section) {
    section.title = event.target.value;
    this.updateSection({ updateData: section,isPreview: true});
  }

  updateSection(data) {
    const { updatePage } = this.props;
    const { section } = this.state;
    updatePage({...data, sectionId: section._id});
  }

  removeQuestion(data) {
    const { section } = this.state;
    // Get the section, then filter out the target
    section.questions = section.questions.filter((question) => question._id !== data.id);
    this.updateSection({
      updateData: section
    })
  }

  render() {
    const { className } = this.props;
    const { section } = this.state;

    // Include all action question card may need
    const functionPackage = {
      add: this.addQuestion,
      update: this.updateSection,
      remove: this.removeQuestion,
    }

    const questionsJSX = section.questions.map((question, index) => (
      <Draggable key={question.title} draggableId={question.title} index={index}>
        {(provided, snapshot) => (
          [(<Element name={question._id} key="element"/>),
          <QuestionCard
            question={question}
            key={question._id}
            provided={provided}
            questionIndex={index}
            functionPackage={functionPackage}/>]
        )}
      </Draggable>
    ))
    return (<div className={classnames(className, styles.sectionContainer)}>
      <TextField
        id="standard-name"
        label="Section Name"
        className={styles.heading}
        defaultValue={section.title}
        onChange={(e) =>this.handleSectionTitleChange(e, section)}
        onBlur={(e) =>this.handleSectionTitleBlur(e, section)}
        margin="normal"
      />
      <Droppable type="card" droppableId={section._id}>
        {(provided, snapshot) => (
          <div className={styles.panelDetailContainer} ref={provided.innerRef}>
            {questionsJSX}
            <div className={styles.questionAddButton}>
              <IconButton color="primary" className={styles.button} component="span" onClick={this.addQuestion}>
                <AddIcon />
              </IconButton>
            </div>
          </div>
        )}
      </Droppable>
    </div>)
  }
}

QuestionSection.propTypes = {
  section: PropTypes.object,
  className: PropTypes.string
}
