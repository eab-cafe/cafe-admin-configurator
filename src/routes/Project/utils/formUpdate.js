import _ from 'lodash';

// This list the level top to down for the tree
const dataMapping = [
  {
    listLabel: "pages",
    idLabel: "pageId"
  }, {
    listLabel: "sections",
    idLabel: "sectionId"
  }, {
    listLabel: "questions",
    idLabel: "questionId"
  }
];

export function formUpdate({form, dataSet}) {
  nodeUpdate({parent: form}, dataSet, 0);
  // This will return object which contain new address.
  // So the reducer and getDerivedStateFromProps will realize
  // the value did change.
  return _.assign({},form);
}

function nodeUpdate({parent}, dataSet, index) {
  const {
    listLabel: currentListLabel,
    idLabel: currentIdLabel } = _.get(dataMapping, index, {});
  const { idLabel: childIdLabel } = _.get(dataMapping, index+1, {});

  const currentId = _.get(dataSet, [currentIdLabel], null);
  const childId = _.get(dataSet, [childIdLabel], null);

  const currentNode = _.find(parent[currentListLabel], {'_id': currentId});
  if ( currentNode ) {
    if ( childId ) {
      nodeUpdate({parent: currentNode}, dataSet, ++index);
    } else {
      // Update the Node
      const currentIndex = _.findIndex(parent[currentListLabel], {'_id': currentId});
      // Replace the target
      const newData = _.assign({},dataSet.data);
      _.set(parent, [currentListLabel, currentIndex], newData);
    }
  }
}
