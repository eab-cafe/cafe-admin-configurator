import React, { Component } from 'react';
import { connect } from 'react-redux';
import classnames from 'classnames';
import styles from './index.module.css';

import { loadDummyData, getProjectPage } from "../../api";
import { projectLoadData, projectUpdateSelectedForm, projectUpdatePreview } from "../../actions/projectAction";

import QuestionForm from "./components/QuestionForm";
import Preview from "./components/Preview";
import { formUpdate } from "./utils/formUpdate";

class Project extends Component{
  constructor(props) {
    super(props);
    this.state = {
      selectedForm : null
    }
    this.formEdit = this.formEdit.bind(this);
    this.dataUpdate = this.dataUpdate.bind(this);
  }

  static getDerivedStateFromProps(nextProps, state) {
    if (nextProps.selectedForm !== state.selectedForm) {
      return {
        selectedForm: nextProps.selectedForm
      }
    }
    return null;
  }

  componentDidMount() {
    // Load API
    const { loadAPIData } = this.props;
    const data = loadDummyData();
    // Load API Data
    getProjectPage({id: "5c89ebea9f90d0751bf172c3"})
      .then((result) => {
        // const { result: pages} = result;
        // const { forms } = data;
        // forms[0].pages[0] = pages
        // loadAPIData(forms);
      });


    // Load Demo Data
    setTimeout(()=>{
      loadAPIData(data.forms);
    }, 1000)
  }

  formEdit(data) {
    this.setState(data);
  }

  dataUpdate(data) {
    const { selectedForm } = this.state;
    const { updateSelectedForm, updatePreview } = this.props;
    const newData = formUpdate({form: selectedForm, rawData: data});
    // updatePreview will only update the preview.
    // updateSelectedForm will update preview and selectedForm.
    // Use updateSelectedForm to save data.
    // Use updatePreview to do living reload in preview.
    if (data.isPreview) {
      updatePreview(newData);
    } else {
      updateSelectedForm(newData);
    }
  }

  render () {
    const { className } = this.props;
    const { selectedForm } = this.state;
    return (
      <div className={classnames(className, styles.projectContainer)}>
        <QuestionForm className={styles.questionForm} selectedForm={selectedForm} formEdit={this.formEdit} dataUpdate={this.dataUpdate}/>
        <Preview className={styles.preview}/>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  selectedForm: state.projectReducer.selectedForm
});

const mapDispatchToProps = dispatch => {
  return {
    loadAPIData: (data) => dispatch(projectLoadData(data)),
    updateSelectedForm: (data) => dispatch(projectUpdateSelectedForm(data)),
    updatePreview: (data) => dispatch(projectUpdatePreview(data)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Project);
