import demoData from "./dummyData/demo_data_v1.json";
import axios from "axios";

axios.defaults.baseURL = 'http://api-dev.eab-cafe.intraeab';

export function loadDummyData() {
  return demoData;
}

export function getProjectPage({id}) {
  return axios.get(`/page/${id}`)
    .then((result) => result.data)
    .catch(()=>{console.log("Fail to load Project Page")});
}
