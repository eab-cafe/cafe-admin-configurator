import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore } from 'redux';

import rootReducer from './reducers';
import App from './App';
import './index.css';
import './theme/default.css';
import './theme/custom.css';
import './assets/icomoon/style.css';

const store = createStore(rootReducer);

ReactDOM.render(
  <Provider store ={store}>
    <App />
  </Provider >
, document.getElementById('root'));
