import textStore from './textStore';
import _ from 'lodash';

const notFoundError = path => {
  throw new Error(`text is not Found. path: ${path}`)
}

export default function i18n({path}) {
  let textData = "";

  textData = _.get(textStore, path, null);

  if (textData === null || typeof textData !== "string") {
    notFoundError(path);
  }

  return textData;
}
