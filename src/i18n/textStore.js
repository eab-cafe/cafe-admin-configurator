export default {
  button: {
    submit: "Submit"
  },
  questionForm: {
    title: {
      label: "Title",
      defaultValue: "Untitled Form",
      placeHolder: "Enter Title"
    },
    description: {
      label: "Description",
      defaultValue: "Description",
      placeHolder: "Enter Description"
    },
    questionTitle: {
      label: "Question",
      defaultValue: "",
      placeHolder: "Question"
    }
  },
  questionTypeContent: {
    none: "Choose question type on the top right to continue."
  },
  questionType: {
    SECTION: "Section",
    TITLE: "Title",
    TEXT_INPUT: "Text Answer",
    SELECTOR: "Single Choice",
    CHECK_BOX: "Check Box",
    DROP_DOWN: "Drop Down List",
    DATE: "Date",
    SCALE: "Scale",
    SLIDER: "Slider",
    FILE_UPLOAD: "File Upload",
    NONE: "Question Type"
  },
  preview: {
    question: {
      yourAnswer: "Your Answer"
    }
  }
}
