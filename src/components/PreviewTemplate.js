import React from 'react';
// import React, { useState } from 'react';
// import Tabs from '@material-ui/core/Tabs';
// import Tab from '@material-ui/core/Tab';
import classnames from 'classnames';
import _ from 'lodash';
import Button from '@material-ui/core/Button';
import Divider from '@material-ui/core/Divider';

import styles from './PreviewTemplate.module.css';
import i18n from '../i18n';
import PreviewTemplateQuestion from './PreviewTemplateQuestion';

function templateSection(section) {
  const filteredQuestion = section.questions.filter((question) => !question.isDisable);
  const questionMappingJSX = _.map(filteredQuestion, (question, index) => (
    <PreviewTemplateQuestion question={question} questionIndex={index} key={question._id}/>
  ));

  return [
    (<div className={styles.section} key={section._id}>
      <div className={styles.sectionTitle}>
        {section.title}
      </div>
      {questionMappingJSX}
    </div>),
    <Divider className={styles.divider} key={`${section._id}_divider`}/>
  ];
}

function templatePage(page) {
  const sectionMappingJSX = _.map(page.sections, (section) => (
    templateSection(section)
  ));

  return (<div className={styles.pageContent}>
    {sectionMappingJSX}
  </div>);
}

export default function PreviewTemplate (props){
  const { preview, isFullscreen } = props;
  const { pages } = preview;
  const tabIndex = 0;
  // const [tabIndex, setTabIndex] = useState(0);

  // function handleTabChange (event, newValue) {
  //   setTabIndex(newValue);
  // }

  // const TabMappingJSX = pages.map((page)=>(
  //   <Tab label={page.title} key={page._id} />
  // ))

  return (
    <div className={classnames(props.className,
      styles.previewContainer,
      { [styles.fullscreenPreview]: isFullscreen})}>
      <div className={styles.title}>
        {preview.title}
      </div>
      <div className={styles.description}>
        {preview.description}
      </div>
      {templatePage(pages[tabIndex])}
      <Button variant="contained" color="primary" className={styles.submit} disabled>
        {i18n({path: "button.submit"})}
      </Button>
    </div>
  )
}
