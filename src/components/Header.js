import React, { Component } from 'react';
import classnames from 'classnames';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import AppsIcon from '@material-ui/icons/Apps';

import styles from './Header.module.css';

export default class Header extends Component {
  render () {
    const {className} = this.props;
    return (
      <AppBar className={classnames(className, styles.header)} position="sticky">
        <Toolbar>
          <IconButton className={styles.menuButton} color="inherit" aria-label="Menu">
            <AppsIcon />
          </IconButton>
        </Toolbar>
      </AppBar>
    )
  }
}
