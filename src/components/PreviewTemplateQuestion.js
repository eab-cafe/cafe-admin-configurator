import React, { Component } from 'react';
import { connect } from 'react-redux';
import classnames from 'classnames';
import Radio from '@material-ui/core/Radio';
import TextField from '@material-ui/core/TextField';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import _ from 'lodash';
import { Element, scroller } from 'react-scroll';

import styles from './PreviewTemplateQuestion.module.css';
import { SECTION, TITLE, TEXT_INPUT, SELECTOR, CHECK_BOX, DROP_DOWN, DATE, SCALE, SLIDER, FILE_UPLOAD } from '../constants/questionType.js';
import i18n from '../i18n';

function textInputQuestion(question) {
  const { type = ""} = question.answers;
  switch (type) {
    case "TEXT_INPUT":
      return (
        <TextField
          className={styles.textField}
          InputProps={{
            classes: {
              input: styles.textFieldInput,
            },
          }}
          placeholder={i18n({path: "preview.question.yourAnswer"})}
          defaultValue= ""
          margin="none"
        />
      );
    case "TEXT_FIELD":
      return (
        <TextField
          className={styles.textField}
          InputProps={{
            classes: {
              input: styles.textFieldInput,
            },
          }}
          placeholder={i18n({path: "preview.question.yourAnswer"})}
          defaultValue= ""
          margin="none"
          multiline
          variant="outlined"
          fullWidth
        />
      );
    default:
      return (<div/>)
  }
}

function selectorQuestion(question) {
  const choices = _.get(question, "answers.choices", []);

  const radioGroupJSX = choices.map((choice) => (
    <FormControlLabel
      className={styles.formControlLabel}
      value={choice.text}
      control={
        <Radio className={styles.radio}
                fontSize="small"/>
      }
      label={choice.text}
      key={choice.text}/>
  ));

  return (
    <FormControl className={styles.formControl}>
      <RadioGroup className={styles.radioGroup}>
        {radioGroupJSX}
      </RadioGroup>
    </FormControl>
  )
}

function questionTypeController(question) {
  const { questionType } = question;
  switch (questionType) {
    case SECTION:
    case TITLE:
    case TEXT_INPUT:
      return textInputQuestion(question);
    case SELECTOR:
      return selectorQuestion(question);
    case CHECK_BOX:
    case DROP_DOWN:
    case DATE:
    case SCALE:
    case SLIDER:
    case FILE_UPLOAD:
    default:
      return null;
  }
}

class PreviewTemplateQuestion extends Component {
  constructor(props) {
    super(props);
    this.mouseClickQuestion = this.mouseClickQuestion.bind(this);
  }

  mouseClickQuestion() {
    const { question } = this.props;
    scroller.scrollTo(`${question._id} card`, {
      duration: 400,
      smooth: true,
      offset: -(window.innerHeight / 2),
      containerId: 'QuestionFormSection'
    });
  }

  render() {
    const { question, questionIndex, className, focusQuestion } = this.props;
    let focusStyle = "";
    if (focusQuestion) {
      if (focusQuestion._id === question._id) {
        focusStyle = styles.highlight;
      } else {
        focusStyle = styles.fade;
      }
    }

    return (<div className={classnames(className ,styles.question, focusStyle)} onClick={() => this.mouseClickQuestion()}>
      <Element name={question._id} className={styles.questionNumber}>
        {`Question ${questionIndex + 1}`}
      </Element>
      <div className={styles.questionTitle}>
        {question.isMandatory ?
          (<span className={styles.mandatory}>*</span>)
          : null}
        {question.title}
      </div>
      <div className={styles.questionContent}>
        {questionTypeController(question)}
      </div>
    </div>)
  }
}

const mapStateToProps = (state) => ({
  focusQuestion: state.previewReducer.focusQuestion,
});

// Force The component to re-render no matter Own Props will change or not
export default connect(mapStateToProps, null, null, {areOwnPropsEqual: () => false})(PreviewTemplateQuestion);
