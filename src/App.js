import React from 'react';
// import { Router } from "@reach/router";

import { PROJECT_ROUTE } from "./constants/routePath";
import Main from "./routes/Main";
// import Home from "./routes/Home";
import Project from "./routes/Project";

export default function App(){
  return (
    <Main path="/">
      <Project path={PROJECT_ROUTE}/>
    </Main>
  );
}

// Later may use
// export default function App(){
//   return (
//     <Router>
//       <Main path="/">
//         <Home path={HOME_ROUTE}/>
//         <Project path={PROJECT_ROUTE}/>
//       </Main>
//     </Router>
//   );
// }
